package com.sampleDocker.smpd.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @GetMapping(value = "/hello")
    public String welcome(){
        return "Welcome to the Spring boot Application Two ..!!";
    }

    @GetMapping(value = "/")
    public String welcomeOne(){
        return "Welcome to the Spring boot Application ..!!";
    }

    @PostMapping(value = "/save/{name}")
    public String saveName(@PathVariable String name){
        return "Hello "+name;
    }

}
