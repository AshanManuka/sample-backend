package com.sampleDocker.smpd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SmpdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmpdApplication.class, args);
	}

}
